import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class NetworkService {

  public networkAlert: any;

  constructor(
    private alertController: AlertController,
    private translateService: TranslateService,
    private network: Network,
  ) {
  }

  public noConnection() {
    console.log(this.network.type);
    console.log(this.network.type == null);
    if (this.network.type == null) {
      return false;
    }
    return this.network.type == 'none';
  }

  public showNetworkAlert() {
    this.translateService.get(['imsafehttp_networkNoInternetTitle', 'imsafehttp_noInternetText', 'const_okText']).subscribe((words) => {
      let networkAlert = this.alertController.create({
        title: words.imsafehttp_networkNoInternetTitle,
        message: words.imsafehttp_noInternetText,
        buttons: [
          {
            text: words.const_okText,
            handler: () => {
              // this.showSettings();
            }
          }
        ]
      });
      networkAlert.present(networkAlert);
    });
  }

  public showErrorAlert(error) {
    let alert;
    this.translateService.get(['imsafehttp_networkErrorTitle', 'imsafehttp_networkErrorText', 'const_okText', 'imsafehttp_network403Title', 'imsafehttp_network403Text']).subscribe((words) => {
      let inputs = [];
      let message: string;
      let title: string;
      if (error.status == 403) {
        title = words.imsafehttp_network403Title;
        message = words.imsafehttp_network403Text;
      }
      else {
        title = words.imsafehttp_networkErrorTitle;
        message = words.imsafehttp_networkErrorText;
        // TODO log error
      }

      alert = this.alertController.create({
        title: title,
        message: message,
        inputs: inputs,
        buttons: [
          {
            text: words.const_okText,
            handler: () => {
              // this.showSettings();
            }
          }
        ]
      });
    });
    alert.present(alert);
    return alert;
  }
}
