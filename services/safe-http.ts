import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import { LoadingController } from 'ionic-angular';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { NetworkService } from '../services/network';

@Injectable()
export class SafeHttp {

  // 403 error event, using ReplaySubject. See http://stackoverflow.com/questions/34376854/delegation-eventemitter-or-observable-in-angular2/35568924#35568924
  private _error403 = new ReplaySubject<any>();
  error403$ = this._error403.asObservable();

  constructor(
    private http: Http,
    private loadingController: LoadingController,
    private networkService: NetworkService) {
  }

  public get(url: string, options?: RequestOptionsArgs, shouldLoad: boolean = true) {
    return this.serverAction('get', url, shouldLoad, options).then(response => response.json());
  }

  public post(url: string, body: string, options?: RequestOptionsArgs, shouldLoad: boolean = true) {
    return this.serverAction('post', url, shouldLoad, options, body).then(response => response.json());
  }

  public patch(url: string, body: string, options?: RequestOptionsArgs, shouldLoad: boolean = false) {
    return this.serverAction('patch', url, shouldLoad, options, body).then(response => response.json());
  }

  public delete(url: string, options?: RequestOptionsArgs, shouldLoad: boolean = false) {
    return this.serverAction('delete', url, shouldLoad, options).then(response => response.json());
  }

  private serverAction(action: string, url: string, shouldLoad: boolean = true, options?: RequestOptionsArgs, body?: string) {
    let loading;
    if (shouldLoad) {
      loading = this.startLoading();
    }

    if (this.networkService.noConnection()) {
      return new Promise<any>((resolve, reject) => {
        if (loading != null) {
          loading.dismiss().then(() => {
            this.networkService.showNetworkAlert();
            reject('noConnection');
          });
        }
        else {
          this.networkService.showNetworkAlert();
          reject('noConnection');
        }
      });
    } else {
      return new Promise<any>((resolve, reject) => {
        let functionCall;
        switch (action) {
          case 'get':
            functionCall = () => this.http.get(url, options);
            break;

          case 'post':
            functionCall = () => this.http.post(url, body, options);
            break;

          case 'patch':
            functionCall = () => this.http.patch(url, body, options);
            break;

            case 'delete':
              functionCall = () => this.http.delete(url, options);
              break;
        }
        functionCall().toPromise()
          .then(response => {
            this.delayedResponse(response, loading, resolve);
          })
          .catch(error => {
            this.onError(error, loading, reject);
          });
      });
    }
  }

  private startLoading(): any {
    let loading = this.loadingController.create();
    loading.present(loading);
    console.log('loading');
    return loading;
  }

  private delayedResponse(response: any, loading: any, resolve: any) {
    if (loading != null) {
      loading.dismiss().then(() => {
        resolve(response);
      });
    }
    else {
      resolve(response);
    }
  }

  private onError(error: any, loading: any, reject: any) {
    if (loading != null) {
      loading.dismiss().then(() => {
        this.handleError(error, reject);
      });
    }
    else {
      this.handleError(error, reject);
    }
  }

  private handleError(error: any, reject: any) {
    if (error.status != 403 && error.status != 409 && error.status != 400) {
      this.networkService.showErrorAlert(error);
    }
    else if (error.status == 403) {
      console.log(error.url);
      if (error.url.indexOf('login') >= 0 || error.url.indexOf('signup') >= 0 || error.url.indexOf('reset_password') >= 0) {
        // Error from login page
      } else {
        this.networkService.showErrorAlert(error).onDidDismiss(() => {
          this._error403.next(error);
        });
      }
    }
    reject(error);
  }

}
