import { NgModule, ModuleWithProviders } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { NetworkService } from './services/network';
export { NetworkService } from './services/network';
import { SafeHttp } from './services/safe-http';
export { SafeHttp } from './services/safe-http';

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule],
  declarations: [
  ],
  exports: []
})
export class IMSafeHttpModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: IMSafeHttpModule,
      providers: [NetworkService, SafeHttp]
    };
  }
}
