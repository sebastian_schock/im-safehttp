### Translation keys

| Key     | Description |
| :------- | ----- |
| imsafehttp_networkNoInternetTitle | networkNoInternetTitle |
| imsafehttp_noInternetText         | noInternetText |
| imsafehttp_networkErrorTitle      | networkErrorTitle|
| imsafehttp_networkErrorText | networkErrorText  |
| imsafehttp_network403Title |  network403Title |
| imsafehttp_network403Text | network403Text |
